//
//  weatherStructure.swift
//  weaterLopez
//
//  Created by KENNY LOPEZ on 29/11/18.
//  Copyright © 2018 kl. All rights reserved.
//

import Foundation
import ObjectMapper

class weatherStructure:Mappable {
    
    var weather:[SingleWeather]?
    var weatherId:Int?
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        weatherId <- map["id"]
        name <- map["name"]
    }
    
}

class SingleWeather:Mappable {
    var weather:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weather <- map["main"]
    }
}
