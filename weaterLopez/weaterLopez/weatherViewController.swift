//
//  weatherViewController.swift
//  weaterLopez
//
//  Created by KENNY LOPEZ on 29/11/18.
//  Copyright © 2018 kl. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire

class weatherViewController: UIViewController {

    @IBOutlet weak var latTextField: UITextField!
    
    @IBOutlet weak var lonTextField: UITextField!
    
    @IBOutlet weak var weatherLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func getWeatherButtonPressed(_ sender: Any) {
        let URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(latTextField.text ?? "35")&lon=\(lonTextField.text ?? "139")&appid=689afd359a83cfa4465f194de69ed630"
        print (URL)
        Alamofire.request(URL).responseObject {(response:DataResponse<weatherStructure>) in
            self.weatherLabel.text = response.value?.weather?[0].weather ?? ""
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
